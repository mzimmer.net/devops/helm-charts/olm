#!/usr/bin/env ash
set -eux

version="${1}"
workspace="${2}"

sed -i "s/^[Nn]ame: olm\$/name: olm-crds/" "${workspace}/deploy/chart/Chart.yaml"
if grep -qcF -m 1 'version:' "${workspace}/deploy/chart/Chart.yaml"
then
  sed -i "s/^[Vv]ersion:.*\$/version: ${version}/" "${workspace}/deploy/chart/Chart.yaml"
else
  echo "version: ${version}" >> "${workspace}/deploy/chart/Chart.yaml"
fi
sed -i "s/^[Dd]escription:.*\$/description: Helm chart for deploying OLM CRDs/" "${workspace}/deploy/chart/Chart.yaml"
truncate -s 0 "${workspace}/deploy/chart/values.yaml"
rm -rf "${workspace}/deploy/chart/templates"
